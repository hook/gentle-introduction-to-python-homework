#!/usr/bin/python

# This program runs 10-times over n (which is set to 10) and if it is even, divides it by two, and if it is odd, adds one.

n = 10	# this is now the number we're actually working on
i = 10	# this is how many times we'll repeate this function

while i > 0:
	print n
	if n % 2 == 0:
		n = n / 2
	else:
		n = n + 1
	i = i - 1	# added a stop condition
