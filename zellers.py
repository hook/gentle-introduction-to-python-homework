#
# Uses the Zeller's algorithm to calculate the week number of your birthdate.
#

print("Enter your name.")

name = raw_input("First name: ")
surname = raw_input("Last name: ")

print("Enter your date of birth (all in numbers):")

day = int(raw_input("Day? "))
month = int(raw_input("Month? "))
year = int(raw_input("Year? "))

# Zeller's algorithm follows from here to final result of 'R'.
if month < 3:
    A = month + 10
    C = year % 100 - 1
else:
    A = month - 2
    C = year % 100

B = day
D = year // 100

W = (13 * A -1) // 5
X = C // 4
Y = D // 4
Z = W + X + Y + B + C - (2 * D)
R = Z % 7

weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

weekday = weekdays[R]

print "Dear,", name, surname, "you were born on a" , weekday + "."
