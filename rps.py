print("Both player's please enter your choices.")

choices = ["rock", "paper", "scissors"]

p1 = raw_input("Player 1' choice: ")

while p1 not in choices:
    p1 = raw_input("Player 1, please enter a valid choice (rock, paper, scissors): ")

p2 = raw_input("Player 2' choice: ")

while p2 not in choices:
    p2 = raw_input("Player 2, please enter a valid choice (rock, paper, scissors): ")

win1 = "Player 1 wins!"
win2 = "Player 2 wins!"
tie = "It is a tie!"

# If both say the same it's a tie.
if p1 == p2:
    print(tie)
# Player 1 says rock
elif (p1 == "rock" and
    p2 == "paper"):
    print(win2)
elif (p1 == "rock" and
    p2 == "scissors"):
    print(win1)
# Player 1 says paper
elif (p1 == "paper" and
    p2 == "rock"):
    print(win1)
elif (p1 == "paper" and
    p2 == "scissors"):
    print(win2)
# Player 1 says scissors
elif (p1 == "scissors" and
    p2 == "rock"):
    print(win1)
elif (p1 == "scissors" and
    p2 == "paper"):
    print(win1)
else:
    print("Something went terribly wrong.")
