#!/usr/bin/python

# This program runs until i is negative.	

i = 10

while i > 0:
	print i
	if i % 2 == 0:
		i = i / 2	# i gets smaller, due to division
	else:
		i = i - 1	# i gets smaller *now*, due to substraction
