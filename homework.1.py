#
# Prints an tic-tac-toe field.
#

a = "  |  | "
b = "--------"

print(a)
print(b)
print(a)
print(b)
print(a)

#
# Some math equations.
#

# Notice that if you have one float, you get a float result. If you have only integers, your result will be an integer and truncate the remainder.
# I.1
print(5/2)
print(5/2.0)
print(5.0/2)

# I.2
print(7 * (1 / 2))
print(7 * (1 / 2.0))

# I.3
print(5 ** 2)
print(5.0 ** 2)
print(5 ** 2.0)

# I.4
# Notice that the last digit is rounded. Python does that for non-terminating/infinite numbers.
print(1 / 3.0)

# II.1
c = (3 * 5) / (3 + 2.0)
print(c)

# II.2
d = ((7.0 + 9.0) ** (1.0/2.0)) * 2.0
print(d)

#II.3
e = (4 - 7) ** 3.0
print(e)

# II.4
f = (-19.0 + 100.0) ** 1.0/4.0
print(f)

# II.5
g = 6 % 4
print(g)

# III
h = 1 - 2 * 3
i = (1 - 2) * 3
print(h)
print(i)

#
# User input
#

first_name = raw_input("Enter your first name: ")
last_name = raw_input("Enter your last name: ")
print("Enter your date of birth:")
month = raw_input("Month? ")
day = raw_input("Day? ")
year = raw_input("Year? ")

print first_name, last_name, "was born on", month, day + ",", year + "."
