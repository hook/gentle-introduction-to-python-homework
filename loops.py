#
# Decimal equivalents for 1/1 to 1/10.
#

for x in range(1,11):
    print(1.0/x)

#
# Countdown from user-inputted number.
#

n = int(raw_input("Enter a number: "))

if n < 0:       # Negative numbers.
    while n <= 0:
        print(n)
        n += 1
else:           # Positive numbers.
    while n >= 0:
        print(n)
        n -= 1

#
# Calculates expotentials of base and exponent given by the user.
#

base = int(raw_input("Enter the base: "))
exp = int(raw_input("Enter the exponent: "))

print(base**exp)

#
# Bothers the user to input an even number, until he finally does so.
#

number = int(raw_input("Enter an even number: "))

while number % 2 != 0:
    print("This number is as odd as you are!")
    number = int(raw_input("Enter an even number now, please: "))

print("Finally you got it.")
