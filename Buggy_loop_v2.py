#
# Assuming the idea of the buggy code was to do as many iterations of dividing 'i' with '2' until 'i' reaches 0 and then stop.
#

# n = 10        # In this case 'n' is not needed.
i = 10

while i > 0:
    print i
    if i % 2 == 0:
        i = i // 2
    else:
        i = i - 1   # Because 'i' is always getting smaller, it will at some point reach 0 and exit the loop.
